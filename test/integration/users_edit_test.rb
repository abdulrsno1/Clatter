require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  def setup 
    @user = users(:abdul)
  end
  
  test 'unsuccesful edit' do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), user: {name: "", email: "test@invalid", password: "test", password_confirmation: "not" }
  end
  
  test 'successful edit' do 
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    name = "Foo bar"
    email = "foo@bar.com"
    patch user_path(@user), user: {name: name, email: email, password: "", password_confirmation: ""}
    
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal name, @user.name
    assert_equal email, @user.email
  end
  test 'successful edit with forwading' do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    name = "Foo Bar"
    email = "foo@bar.com"
    patch user_path(@user), user: {name: name, email: email, password: "", password_confirmation: ""}
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal name, @user.name
    assert_equal email, @user.email
  end
end
